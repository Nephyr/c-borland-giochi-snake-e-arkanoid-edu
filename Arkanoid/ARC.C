//----------------------------------------------------------------------------->
//      IDE: Borland
//      Copyright 2008 - Sbrissa, Vanzo, Cerizza
//----------------------------------------------------------------------------->

#include<graphics.h>
#include<stdlib.h>
#include<conio.h>
#include<stdio.h>
#include<bios.h>
#include<math.h>
#include<dos.h>
#include<time.h>
#define GRAD 57.806
#define A 299

int xp[A],yp[A],tp[A];
int c[6]={0,0,0,0,0,0};
void Rndm();
int NuovoGioco();
int Classifica(int);
int Help();
int Cinema();

void main()
{
	char ch;
	int edC;
	int gdriver=DETECT,gmode;
	initgraph(&gdriver,&gmode,"C:\\Borlandc\\bgi");
	do
	{
		setcolor(15);
		settextjustify(CENTER_TEXT, CENTER_TEXT);
		settextstyle(TRIPLEX_FONT, HORIZ_DIR, 9);
		outtextxy(getmaxx()/2, 60, "ARKANOID");
		line(30, 120, getmaxx()-30, 120);
		printf("\n\n\n\n\n\n\n\n\n   \t * Premere [1] per Nuovo Gioco.");
		printf("\n   \t * Premere [2] per visualizzare la Classifica.");
		printf("\n   \t * Premere [3] per visualizzare la Funzione Cinema.");
		printf("\n   \t * Premere [4] per visualizzare l'Help.");
		printf("\n   \t * Premere [5] per Chiudere il Gioco.");
		while(bioskey(1)==0){	/* LOOP BK */	}
		ch=bioskey(0);
		switch(ch)
		{
			case '1':
				NuovoGioco();
				edC = 1;
			break;
			case '2':
				Classifica(0);
				edC = 1;
			break;
			case '3':
				Cinema();
				edC = 1;
			break;
			case '4':
				Help();
				edC = 1;
			break;
			case '5':
				edC = 0;
			break;
			default:
				edC = 1;
			break;
		}
		clrscr();
		cleardevice();
	}while(edC == 1);
}

int Cinema()
{
	float i,d;
	clrscr();
	cleardevice();
	setcolor(15);
	settextjustify(CENTER_TEXT, CENTER_TEXT);
	settextstyle(TRIPLEX_FONT, HORIZ_DIR, 9);
	outtextxy(getmaxx()/2, 60, "ARKANOID");
	line(30, 120, getmaxx()-30, 120);
	for(i=0;i<60;i=i+0.1)
	{
		delay(2);
		d=floor(-i*6);
		line(getmaxx()/2,240,100*sin((d*6.28)/360)+getmaxx()/2,100*cos((-d*6.28)/360)+240);
	}
	return 1;
}

int NuovoGioco()
{

	int a=300,b=0,point=0,p=0,h=0,Color=16,daly=12,white=0,dimrac=120,Count=0,WN,xball=0,yball=0,x,y,xball2,yball2,me=0,te=0;
	char z='c',msg[80],str[2]={'1','\0'};
	float ang;
	unsigned long int i,k,j,l;
	int gdriver=DETECT,gmode;
	initgraph(&gdriver,&gmode,"C:\\Borlandc\\bgi");
	Rndm();
	ang=30;
	for(b=6;b!=0;b--)
	{
		setcolor(b+8);
		str[0]=(b-1)+48;
		settextjustify(CENTER_TEXT, CENTER_TEXT);
		settextstyle(TRIPLEX_FONT, HORIZ_DIR, 9);
		if(b!=1)
		{
			outtextxy(getmaxx()/2, (getmaxy()/2),str);
		}
		else
		{
			outtextxy(getmaxx()/2, (getmaxy()/2),"GO!");
		}
		delay(900);
		printf("\a");
		cleardevice();
	}
	setcolor(15);
	bar(a,455,dimrac+a,460);
	setcolor(15);
	line(15,15,625,15);
	line(15,15,15,465);
	line(15,465,625,465);
	line(625,15,625,465);
	for(b=0;b<A;b++)
	{
		if(xp[b]==22)
		{
			Color=Color-1;
		}
		if(tp[b]!=0 && white==0)
		{
			setcolor(Color);
			rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
		}
		else
		{
			setcolor(0);
			rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
		}
	}
	do
	{
		while( bioskey(1) == 0 )
		{
			xball=me+320;
			yball=240-te;
			ang=ang/GRAD;
			yball=floor(tan(ang)*xball)+yball;
			setcolor(15);
			circle(me+320,240-te,3);
			circle(me+320,240-te,2);
			delay(daly);
			setcolor(0);
			circle(me+320,240-te,3);
			circle(me+320,240-te,2);
			if(p==0)
			{
				me+=1;
			}
			if(p==1)
			{
				me-=1;
			}
			if(h==0)
			{
				te+=1;
			}
			if(h==1)
			{
				te-=1;
			}
			if(yball<20)
			{
				h=1;
			}
			if(yball>448 && (xball>a && xball<a+dimrac))
			{
				h=0;
			}
			if(yball>460 && (xball<a || xball>a+dimrac))
			{
				cleardevice();
				printf("\n\n\n\n\t\t Hai Perso... Ritenta!\n\t\t Copyright 2008 - Sbrissa, Vanzo, Cerizza");
				getch();
				Classifica(point);
				exit(1);
			}
			else
			{
				WN=1;
				for(b=0;b<A;b++)
				{
					if(tp[b]!=0){ WN=0; }
				}
				if(WN==1)
				{
					cleardevice();
					printf("\n\n\n\n\t\t Hai Vinto... Congratulazione!\n\t\t Copyright 2008 - Sbrissa, Vanzo, Cerizza");
					getch();
					Classifica(point);
					exit(1);
				}
			}
			if(xball<21)
			{
				p=0;
			}
			if(xball>619)
			{
				p=1;
			}
			white=1;
			for(b=0;b<A;b++)
			{
				if(dimrac>25 && Count==16){ Count=0; dimrac-=10; if(daly>10) daly-=1; }
				if(tp[b]!=0)
				{
					//----ORIZZONTALI---->

					if(yball-4==yp[b]+10 && xball>=xp[b] && xball<=xp[b]+24)
					{
						tp[b]=(int)0;
						h=1;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
					if(yball-4==yp[b]-10 && xball>=xp[b] && xball<=xp[b]+24)
					{
						tp[b]=(int)0;
						h=0;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}

					//----LATERALI---->

					if(xball+4==xp[b] && yball>=yp[b] && yball<=yp[b]+10)
					{
						tp[b]=(int)0;
						p=1;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
					if(xball-4==xp[b]+24 && yball>=yp[b] && yball<=yp[b]+10)
					{
						tp[b]=(int)0;
						p=0;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}

					//----ANGOLI---->

					if( (xball<=xp[b]+2 && xball>=xp[b]-2) && (yball>=yp[b]-2 && yball<=yp[b]) )
					{
						tp[b]=(int)0;
						h=1;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
					if( (xball<=xp[b]+26 && xball>=xp[b]+22) && (yball>=yp[b]-2 && yball<=yp[b]) )
					{
						tp[b]=(int)0;
						h=0;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
					if( (xball<=xp[b] && xball>=xp[b]-4) && (yball>=yp[b]+10 && yball<=yp[b]+12) )
					{
						tp[b]=(int)0;
						p=1;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
					if( (xball<=xp[b]+26 && xball>=xp[b]+22) && (yball>=yp[b]+10 && yball<=yp[b]+12) )
					{
						tp[b]=(int)0;
						p=0;
						Count+=1;
						setcolor(0);
						point+=5;
						rectangle(xp[b],yp[b],xp[b]+24,yp[b]+11);
					}
				}
			}
			setfillstyle(0, getmaxcolor());
			if((z=='a')&&(a>=30))
			{
				bar(20,455,620,460);
				a-=10;
				z = 'c';
				setfillstyle(1, getmaxcolor());
				bar(a,455,dimrac+a,460);
			}
			if((z=='d')&&(a+dimrac<=610))
			{
				bar(20,455,620,460);
				a+=10;
				z = 'c';
				setfillstyle(1, getmaxcolor());
				bar(a,455,dimrac+a,460);
			}
		}
		z=bioskey(0);
		// ----CHEAT---->

		if(z=='+' && dimrac!=120){ dimrac=120; }
		if(z=='-' && dimrac!=25){ dimrac=25; }

		// ----CHEAT---->
	}while(z!='q');
}

int Classifica(int d)
{
	int WE=1, tmp, n, i;
	clrscr();
	cleardevice();
	setcolor(15);
	settextjustify(CENTER_TEXT, CENTER_TEXT);
	settextstyle(TRIPLEX_FONT, HORIZ_DIR, 9);
	outtextxy(getmaxx()/2, 60, "ARKANOID");
	line(30, 120, getmaxx()-30, 120);
	printf("\n\n\n\n\n\n\n\n\n");
	if(d==0)
	{
		for(i=0 ; i<6 ; i++)
		{
			if(c[i]!=0){ printf("\t   %d - %d Punti\n", i+1, c[i]); }
		}
		getch();
		return 1;
	}
	else
	{
		WE=1;
		for(i=0 ; i<6 ; i++)
		{
			if(c[i]==0 && WE==1){ c[i]=d; WE=0; }
		}
		if(c[5]!=0){ c[5]=d; }
		for(n=0 ; n<6 ; n++)
		{
			for(i=n ; i<6 ; i++)
			{
				tmp = c[n];
				if(tmp < c[i])
				{
					c[n] = c[i];
					c[i] = tmp;
				}
			}
		}
		for(i=0 ; i<6 ; i++)
		{
			if(c[i]!=0){ printf("\t   %d - %d Punti\n", i+1, c[i]); }
		}
		getch();
		return 1;
	}
}

int Help()
{
	clrscr();
	cleardevice();
	settextjustify(CENTER_TEXT, CENTER_TEXT);
	settextstyle(TRIPLEX_FONT, HORIZ_DIR, 9);
	outtextxy(getmaxx()/2, 60, "ARKANOID");
	line(30, 120, getmaxx()-30, 120);
	printf("\n\n\n\n\n\n\n\n\n   \t   Comandi ARKANOID\n");
	printf("\t     - A e D per movimenti orizzontali\n");
	printf("\t     - Q per tornare alla schermata principale\n");
	printf("\n\n\t   Comandi 'Escape' Menu\n");
	printf("\t     - 1, 2 e 3 per selezioni Sotto Menu (S. Principale)\n");
	printf("\t     - 4 per uscire dal gioco (S. Principale)\n");
	printf("\t     - Tutti i Tasti per tornare alla schermata principale\n");
	getch();
	return 1;
}

void Rndm()
{
	int b;
	randomize();
	for(b=0;b<A;b++)
	{
		tp[b]=random(14);
		if(b==0)
		{
			xp[b]=22;
			yp[b]=40;
		}
		else
		{
			if(xp[b-1]+26<getmaxx()-27)
			{
				xp[b]=xp[b-1]+26;
				yp[b]=yp[b-1];
			}
			else
			{
				xp[b]=22;
				yp[b]=yp[b-1]+13;
			}
		}
	}
}